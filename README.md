# My dotfiles

Uses [Dotbot](https://github.com/anishathalye/dotbot) to manage dotfiles.

Steps to install on new mac:
1. Clone this repo: `git clone git@gitlab.com:ldgit/dotfiles.git ~/.dotfiles`
2. `cd ~/.dotfiles`
3. Run `./install`
4. Run `./.macos` to set some sensible macOS defaults

The `install` script will: 
1. Install [Homebrew](https://docs.brew.sh/) and apps specified in `Brewfile`.
2. Install [VSCode](https://code.visualstudio.com/) extensions listed in `vscode/extensions`
3. Setup dotfiles in home dir

## Updating `vscode` extensions list and `Brewfile`

Run `./update-backup` to update [Visual Studio Code](https://code.visualstudio.com/) extensions list and `Brewfile`.

## Updating Node.js

Node.js is installed through nvm.

## Upgrading Dotbot

To upgrade [dotbot](https://github.com/anishathalye/dotbot#integrate-with-existing-dotfiles) submodule: 
1. Run `git submodule update --remote dotbot`.
2. Commit changes to the `dotbot` repo:
   1. `git add dotbot`
   2. `git commit -m "Update dotbot"`

## Appendix

Useful links:
- https://nick.balestrafoster.com/2015/Installing-Ruby-Gems-Without-Sudo/


#!/usr/bin/python3

import subprocess
import os
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("--url", "-u", help="Twitter URL", required=True)
parser.add_argument("--metadata", "-m", help="Create a metadata txt file that contains full text of the tweet and tweet URL. Always created if filename is too long.", required=False, action="store_true")
args=parser.parse_args()

twitter_url = args.url
create_metadata = args.metadata

yt_dlp = ["yt-dlp"]
default_arguments = [
  "--cookies", 
  "~/cookies.txt", 
  "--ignore-errors",
  "--geo-bypass", 
  "--audio-quality=0", 
  "--user-agent",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:120.0) Gecko/20100101 Firefox/120.0",
  "--cookies-from-browser",
  "firefox",
  # "--list-formats", # Only uncomment for debugging
]

# yt-dlp usually appends 40+ characters of crap at the end of temporary files, which leads to "filename too long" errors.
# Because of this we use this padding to check if filename + padding is too long.
padding = "paddingpaddingpaddingpaddingpaddingpadding";

# Extract filenames for each video in playlist.
command_output = subprocess.run(yt_dlp + default_arguments + ["--restrict-filenames", "--simulate", "-o", "~/Desktop/%(upload_date)s-%(title)s.%(ext)s", "--get-filename", twitter_url], text=True, stdout=subprocess.PIPE)

playlist_filenames = command_output.stdout.strip().split("\n")

print("[WORKING] Downloading " + str(len(playlist_filenames)) + " videos")

# Extract full title as metadata to a separate file.
def extract_metadata():
  print("[WORKING] Creating metadata txt file")
  command_output = subprocess.run(yt_dlp + default_arguments + ["--simulate", "--get-description", twitter_url], text=True, stdout=subprocess.PIPE)
  with open(os.path.join(filename[:-4][:150] + "_metadata.txt"), 'w') as metadata_file:
    metadata_file.write(command_output.stdout)
    metadata_file.write(twitter_url)

for index, full_filename in enumerate(playlist_filenames):
  filename = full_filename[:-4]
  extension = full_filename[-4:]

  if len(full_filename + padding) > 250:
    print("[WORKING] Filename is too large, using short filenames.")
    subprocess.run(yt_dlp + default_arguments + ["-o", filename[:200] + "_" + str(index + 1) + extension, "-I", str(index + 1), twitter_url], check=True)
    extract_metadata()
  else:
    print("[WORKING] Filename size is OK. Downloading without filename modifications.")
    subprocess.run(yt_dlp + default_arguments + ["-o", filename + extension, "-I", str(index + 1), twitter_url], check=True)
    if create_metadata:
      extract_metadata()

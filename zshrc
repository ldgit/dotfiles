ZSH_DISABLE_COMPFIX=true

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/Users/dlucic/.oh-my-zsh"

NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$PATH:$NPM_PACKAGES/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# ZSH_THEME="robbyrussell"
# ZSH_THEME="spaceship"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh
source /usr/local/share/powerlevel10k/powerlevel10k.zsh-theme

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias dtf="code ~/.dotfiles"
alias cdtf="cd ~/.dotfiles"
alias cdp="cd ~/Documents/Programming"
alias lc="colorls -lA --sd"
alias ytd="yt-dlp --cookies ~/cookies.txt --ignore-errors --geo-bypass --restrict-filenames --audio-quality=0 -f 'best[ext=mp4]/best' -o '~/Desktop/%(upload_date)s-%(title)s.%(ext)s'"
# For Youtube, downloads largest video+audio file, with max resolution being 1440p.
alias ytda="yt-dlp --cookies ~/cookies.txt --ignore-errors --geo-bypass --restrict-filenames -S 'res:1440' --audio-quality=0 -o '~/Desktop/%(upload_date)s-%(title)s.%(ext)s'"
# For Youtube (audio only)
alias ytdam="yt-dlp --cookies ~/cookies.txt --ignore-errors --audio-format mp3/flac --geo-bypass --restrict-filenames --audio-quality=0 --extract-audio -o '~/Desktop/%(upload_date)s-%(title)s.%(ext)s'"
# Process ID is the second value
alias portcheck="lsof -nP -iTCP -sTCP:LISTEN | grep"
# Update brew and brew casks
# - `brew update` gets the information about the most recent versions of the available formulae and casks from the Homebrew servers
# - `brew outdated --greedy` lists the outdated installed formulae and casks, `--greedy` includes in its output the casks that are marked as "auto-updateable"
# - `brew upgrade --greedy` upgrades the packages listed by `brew outdated`.
alias brewup="brew update && brew outdated --greedy && brew upgrade --greedy"
alias ff="find . -name"

# Thank you to https://github.com/wooglie
notify() {
    if [ $# -gt 0 ]; then
        message="$*"
    else
        message="Command finished"
    fi
    osascript -e "display notification \"$message\""
}

# Install npm dependencies from lock file
n() {
    useYarn=./yarn.lock
    useNpm=./package-lock.json
    if [ -f "$useYarn" ]; then
        echo "Removing node-modules"
        rm -rf ./node_modules
        echo "Installing using Yarn"
        yarn install --frozen-lockfile
    elif [ -f "$useNpm" ]; then
        echo "Installing using npm"
        npm ci
    else
        echo "Did not find lock file, aborting!"
    fi
}

yt() {
    if ~/.dotfiles/bin/twitter-downloader.py -u "$*"; then
        echo "Download SUCCESSFUL"
    else
        echo "Download FAILED: $*"
        notify "Download FAILED: $*"
    fi
}

ytm() {
    if ~/.dotfiles/bin/twitter-downloader.py --metadata -u "$*"; then
        echo "Download SUCCESSFUL"
    else
        echo "Download FAILED: $*"
        notify "Download FAILED: $*"
    fi
}

untilfail() {
    while "$@"; do :; done
}

# From here: https://gorails.com/setup/osx/10.15-catalina
export PATH="$HOME/.rbenv/bin:$PATH"
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
source $(dirname $(gem which colorls))/tab_complete.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Must be last plugin (https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md#with-a-plugin-manager)
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Activate zsh completions
if type brew &>/dev/null; then
    FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

    autoload -Uz compinit
    compinit
fi
